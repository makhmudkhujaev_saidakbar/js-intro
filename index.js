//  answer-1
function num(param) {
   if (!isNaN(param) && param !== '') {
      if ((param % 3 === 0) && (param % 5 === 0)) {
         console.log(true);
      }else{
         console.log(false);
      }
   }else{
      console.log('Iltimos faqat son kiriting!');
   }
}

num(90);

// answer-2

function testing(param) {
   if (!isNaN(param) && param !== '') {
      if (param % 2 === 0) {
         console.log("Juft");
      }else{
         console.log("Toq");
      }
   }else{
      console.log('Iltimos faqat son kiriting!');
   }
}

testing(100)

// answer-3

let num = [22, 5, 60, 30, 3021, 4323];

let sortText = [100, 45]

sortText.sort((x, y) => x - y);

console.log(sortText);

// answer-4

let arr = [
   {test: ['a', 'b', 'd', 'c']},
   {test: ['a', 'b']},
   {test: ['a', 'b', 'w', 'c', 'c']},
   {test: ['a', 'b', 'k', 'c']},
]

let sortArr = [];
arr.forEach((param) => {
   param.test.forEach((elem) => {
      if (!sortArr.includes(elem)) {
         sortArr.push(elem);
      }
   })
})
console.log(sortArr.sort());

// answer-5

let obj1 = {num: 1};
let obj2 = {num: 2};

if (JSON.stringify(obj1) === JSON.stringify(obj2)) {
   console.log(true);
}else{
   console.log(false);
}